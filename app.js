const mongoose = require('mongoose');
const config = require('./config/config');

const express = require('express');
const app = express();
const dotenv = require('dotenv');
const bodyParser = require('body-parser');
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./swagger.json');
// const cors = require('cors');
// var os = require('os');
dotenv.config();

/*bring in route*/
const routes = require('./src/routes');

/*middleware*/
// app.use(cors());
app.use(bodyParser.json({limit: '50mb', extended: true}));

// // no mount path; executed for every request.
// app.use(function (req, res, next) {
//     res.set("Access-Control-Expose-Headers", "X-Served-By");
//     res.set("X-Served-By", os.hostname);
//     next();
// });

mongoose.connect(config.mongodb.uri, config.mongodb.options).then(() => console.log('Connect to DB')).catch((err) => console.log('Error', err))

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
app.use('/', routes);

const port = process.env.PORT || 3000;
app.listen(port, () => {
    console.log(`Application is running on port : ${port}`);
})