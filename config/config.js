const Pack = require('../package')

const config = {
    host: process.env.SERVICE_HOST || 'localhost',
    port: process.env.SERVICE_PORT || 3000,
    version: Pack.version,
    mongodb: {
        uri: process.env.MONGODB_URI || 'mongodb://localhost:27017/deni',
        options: {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useFindAndModify: false,
            autoReconnect: true,
            useCreateIndex: true
    }
  },
}

module.exports = config