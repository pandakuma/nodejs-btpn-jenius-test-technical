const Mongoose = require('mongoose')

Mongoose.set('useCreateIndex', true)
const UserSchema = new Mongoose.Schema({
  username: {
    type: String,
    required: true,
  },
  emailAddress: {
    type: String,
    required: true
  },
  accountNumber: {
    type: String,
    max: 15,
    required: true
  },
  identityNumber: {
    type: String,
    max: 15,
    required: true,
    unique: true
  }
}, { collection: 'user', timestamps: {}})

module.exports = {
  schema: UserSchema,
  model: Mongoose.model('User', UserSchema)
}