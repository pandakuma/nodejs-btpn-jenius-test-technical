exports.apiKeyJenius = process.env.API_KEY_JENIUS

exports.apiKey = process.env.API_KEY;

exports.secretKey = process.env.SECRET_KEY

exports.refreshKey = process.env.REFRESH_KEY

exports.responseCodeSuccess = "00"

exports.responseCodeInvalidToken = "UA"

exports.responseCodeInvalidParam = "IF"

exports.responseCodeDataNotFound = "MD"

exports.responseCodeGeneralError = "GE"

exports.responseDescriptionSuccess = "Success"

exports.responseDescriptionInvalidToken = "Invalid/expired token"

exports.responseDescriptionInvalidParam = "Invalid format parameter"

exports.responseDescriptionGeneralError = "General Error"

exports.responseDescriptionDataNotFound = "Data tidak ditemukan"

exports.CRYPTO_ALGORITHM = process.env.CRYPTO_ALGORITHM

exports.CRYPTO_KEY = process.env.CRYPTO_KEY

exports.CRYPTO_IV = process.env.CRYPTO_IV

exports.CRYPTO_ALGORITHM2 = process.env.CRYPTO_ALGORITHM2

exports.CRYPTO_KEY2 = process.env.CRYPTO_KEY2

exports.CRYPTO_IV2 = process.env.CRYPTO_IV2

exports.WEBSITE_URL = process.env.WEBSITE_URL

exports.PATH_FILE = process.env.PATH_FILE

exports.EXPIRED_TOKEN = process.env.EXPIRED_TOKEN
