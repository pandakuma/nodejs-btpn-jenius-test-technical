const userService = require('../../service/UserSerice');
const constants = require('../../common/constant')

module.exports = {
    async findSemua(req, res, next) {

        let data = await userService.findAll()
        if (!data) {
            return res.status(404).send({
                response_code: constants.responseCodeDataNotFound,
                response_message: "Data tidak ditemukan"
            });
        }

        res.send(data)
    },
    async findUserByIdentity(req, res) {
        let identityNumber = req.params.id
        let data = await userService.findOneByIdentityNumber(String(identityNumber))
        if (!data) {
            return res.status(404).send({
                response_code: constants.responseCodeDataNotFound,
                response_message: "Data tidak ditemukan"
            });
        }

        res.send(data)
    },
    async findUserByAccNumber(req, res) {
        let accNumber = req.params.id
        let data = await userService.findOneByAccNumber(accNumber)
        if (!data) {
            return res.status(404).send({
                response_code: constants.responseCodeDataNotFound,
                response_message: "Data tidak ditemukan"
            });
        }

        res.send(data)
    },
    async findOneByUsername(req, res) {
        let username = req
        let data = await userService.findOneByUsername(username)
        if (!data) {
            return res.status(404).send({
                response_code: constants.responseCodeDataNotFound,
                response_message: "Data tidak ditemukan"
            });
        }

        res.send(data)
    },
    async addUser(req, res) {
        let payload = req.body
        let data = await userService.addUser(payload)
        if (!data) {
            return res.status(404).send({
                response_code: constants.responseCodeGeneralError,
                response_message: "Data gagal disimpan"
            });
        }

        res.send(data)
    },
    async editUser(req, res) {
        
        let identityNumber = req.params.id

        const user = await userService.findOneByIdentityNumber(identityNumber)
        if (!user) {
            return res.status(404).send({
                response_code: constants.responseCodeDataNotFound,
                response_message: "Data tidak ditemukan"
            });
        }

        let data = await userService.editUser(user, req.body)
        if (!data) {
            return res.status(404).send({
                response_code: constants.responseCodeGeneralError,
                response_message: "Data gagal diubah"
            });
        }

        res.send(data)
    },
    async deleteUser(req, res) {
        
        let identityNumber = req.params.id

        const user = await userService.findOneByIdentityNumber(identityNumber)
        if (!user) {
            return res.status(404).send({
                response_code: constants.responseCodeDataNotFound,
                response_message: "Data tidak ditemukan"
            });
        }
        
        let data = await userService.deleteUser(identityNumber)
        if (!data) {
            return res.status(404).send({
                response_code: constants.responseCodeGeneralError,
                response_message: "Data gagal dihapus"
            });
        }

        res.send(data)
    }
}