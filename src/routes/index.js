const express = require('express');
const controller = require('../controller');
const { validationHeader } = require('../validation');
const { verifyToken } = require('../validation/ValidationHeader');
const dbcontroller = require('../controller/User')

const router = express.Router();

/* GET home page. */
router.get('/', function (req, res, next) {
    res.header('Content-Type', 'text/html')
        .send("<html><center>Wellcome to Jenius Backend, please use /api-docs after the port(3000) to get a swagger docs. Enjoy it !!!</center></html>");
});

/* Generate Token */
router.post('/generate-token/:id', validationHeader.generateToken);

/* User */
router.get('/user/list-user', validationHeader.verifyApiKeyJenius, validationHeader.verifyToken, controller.User.findSemua);
router.get('/user/list-user-by-identity/:id', validationHeader.verifyApiKeyJenius, controller.User.findUserByIdentity);
router.get('/user/list-user-by-acc/:id', validationHeader.verifyApiKeyJenius, controller.User.findUserByAccNumber);

router.post('/user/add-user', validationHeader.verifyApiKeyJenius, controller.User.addUser);
router.put('/user/edit-user/:id', validationHeader.verifyApiKeyJenius, controller.User.editUser);
router.delete('/user/delete-user/:id', validationHeader.verifyApiKeyJenius, controller.User.deleteUser);

module.exports = router;