const mongoose = require('mongoose')
const user = require('../../models/user')
const User = mongoose.model('User')

async function findAll() {
  let data = await User.find({})
  return data
}

async function findByAccAndIdentity(accNumber,identity) {
  let data = await User.findOne({account_number: accNumber, identity_number: identity})
  return data
}

async function findOneByIdentityNumber(identity) {
  let data = await User.findOne({identityNumber: identity})
  return data
}

async function findOneByAccNumber(accNumber) {
  let data = await User.findOne({accountNumber: accNumber})
  return data
}

async function findOneByUsername(username) {
  let data = await User.findOne({username: username})
  return data
}

async function addUser(payload) {
  var user = new User({
    username: payload.username,
    accountNumber: payload.accountNumber,
    identityNumber: payload.identityNumber,
    emailAddress: payload.emailAddress
  })
  const res = await user.save({ validateBeforeSave: false })
  return res
}

async function editUser(user, payload) {
  
  user.username = payload.username
  user.accountNumber = payload.accountNumber
  user.emailAddress = payload.emailAddress

  console.log("data user update ", user)
  const res = await user.save({ validateBeforeSave: false })
  return res
}

async function deleteUser(identity) {
  
  console.log("==== identityNumber ==== ", identity)
  let data = await User.deleteOne({identityNumber: identity})
  return data
}

module.exports = {
    findByAccAndIdentity,
    findOneByIdentityNumber,
    findOneByAccNumber,
    findOneByUsername,
    findAll,
    addUser,
    editUser,
    deleteUser
  }