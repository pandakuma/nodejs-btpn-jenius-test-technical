const jwt = require('jsonwebtoken');
const { secretKey, apiKeyJenius } = require('../../common/constant');
const constants = require('../../common/constant');
const userService = require('../../service/UserSerice')

module.exports = {

    async generateToken(req, res) {
        identity = req.params.id
        const user = await userService.findOneByIdentityNumber(identity)
        if (!user) {
            return res.status(404).send({
                response_code: constants.responseCodeDataNotFound,
                response_message: "Data tidak ditemukan"
            });
        }

        try {
            /* create accress token */
            const token = 'Bearer ' + jwt.sign({
                username: user.username,
                email: user.emailAddress
            }, secretKey, {
                expiresIn: "2h"
            })

            return res.status(200).json({
                response_code: constants.responseCodeSuccess,
                access_token: token
            });
        } catch (error) {
            console.log(error);
            return res.status(500).json({
                response_code: constants.responseCodeGeneralError,
                response_message_Ind: 'Terjadi kegagalan sistem.',
                response_message_Eng: 'Internal server error.',
                response_message: JSON.stringify(error)
            })
        }
    },

    verifyApiKeyJenius(req, res, next) {

        let apiKey = req.headers['x-api-key'];

        if (!apiKey) {
            return res.status(401).send({
                response_code: constants.responseCodeInvalidParam,
                response_message: "Api key tidak ditemukan"
            });
        }

        if (apiKey !== apiKeyJenius) {
            return res.status(404).send({
                response_code: constants.responseCodeInvalidParam,
                response_message: "Api key tidak ditemukan"
            });
        }

        next();

    },

    verifyToken(req, res, next) {

        let tokenHeader = req.headers['x-access-token'];

        if (!tokenHeader) {
            return res.status(404).send({
                response_code: constants.responseCodeInvalidToken,
                response_message: "Token tidak ditemukan"
            });
        }

        if (tokenHeader.split(' ')[0] !== 'Bearer') {
            return res.status(400).send({
                response_code: constants.responseCodeInvalidToken,
                response_message: "Format token salah"
            });
        }

        let token = tokenHeader.split(' ')[1];

        if (!token) {
            return res.status(403).send({
                response_code: constants.responseCodeInvalidToken,
                response_message: "Token tidak tersedia"
            });
        }

        jwt.verify(token, secretKey, (err, decoded) => {
            if (err) {
                return res.status(400).send({
                    response_code: constants.responseCodeInvalidToken,
                    response_message: "Token yang anda masukan tidak dikenali"
                });
            }
            req.username = decoded.username;

            // Check token user 
            userService.findOneByUsername(req.username).then((user) => {
                if (!user) {
                    return res.status(401).send({
                        response_code: constants.responseCodeDataNotFound,
                        response_message: "User tidak ada"
                    });
                }

                req.authorizedUser = user
                next();
            })
        });
    },
}
